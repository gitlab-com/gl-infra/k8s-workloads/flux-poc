#!/usr/bin/env bash

set -eu
set -o pipefail

# Install kind
curl -fsSL -o kind "https://github.com/kubernetes-sigs/kind/releases/latest/download/kind-linux-amd64"
chmod +x kind
mv kind /usr/local/bin/kind

# Install kubectl
kubectl_version=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)
curl -fsSL -o kubectl "https://storage.googleapis.com/kubernetes-release/release/${kubectl_version}/bin/linux/amd64/kubectl"
chmod +x kubectl
mv kubectl /usr/local/bin/kubectl

# Install flux
flux_tag=$(curl -s "https://api.github.com/repos/fluxcd/flux2/releases/latest" | jq -r '.tag_name')
flux_version=${flux_tag#v}
curl -fsSL "https://github.com/fluxcd/flux2/releases/download/${flux_tag}/flux_${flux_version}_linux_amd64.tar.gz" | tar xz
chmod +x flux
mv flux /usr/local/bin/flux

echo "[INFO] Creating Kubernetes Kind cluster"
kind create cluster --name=sut --wait=60s

# This is a hack!
# When using Docker-in-Docker, the actual Docker server is running outside of the container.
# For kubectl being able to talk to the Kind cluster, we need to use the remote address of the Docker server.
sed -i -e "s/127.0.0.1/docker/g" ~/.kube/config

echo "[INFO] Installing Flux in Kubernetes Kind cluster"
# flux install

echo "[INFO] Setting up cluster reconciliation"
# flux create source git flux-system --interval=10m --url= --branch= --ignore-paths=
# flux create kustomization flux-system --interval=10m --source=flux-system --path=

echo "[INFO] Verifying cluster reconciliation"

echo "[INFO] Verifying tenant reconciliation"
